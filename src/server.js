const express = require('express');
const server = express();
const bot = require('./service/bot')
const bodyparser = require('body-parser');

server.use(bodyparser.json());
server.use("/",bot);

module.exports = server;